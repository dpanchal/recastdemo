#include <iostream>
#include <fstream>

int main(int argc, char** argv) {
    std::ofstream myFile;
    std::cout << "Hello world, we will write a message to " << argv[1] << std::endl;
    myFile.open(argv[1]);
    myFile << "Writing this to a file. My message is " << argv[2] << std::endl;
    myFile.close();
    return 0;
}
